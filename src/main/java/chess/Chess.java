package chess;

import java.util.Scanner;

public class Chess {
    public static void main(String[] args) {
        final String blackField = "\u25A0";
        final String whiteField = "\u25A1";

        Scanner input = new Scanner(System.in);

        System.out.printf("Podaj długość szachownicy");
        int longChess = input.nextInt();

        for (int a = 0; a < longChess; a++) {
            if (a % 2 == 0) {
                for (int i = 0; i < longChess; i++) {
                    if (i % 2 == 0) {
                        System.out.print(" " + blackField);
                    } else {
                        System.out.print(" " + whiteField);
                    }
                }
                System.out.println();
            } else {
                for (int i = 0; i < longChess; i++) {
                    if (i % 2 == 0) {
                        System.out.print(" " + whiteField);
                    } else {
                        System.out.print(" " + blackField);
                    }
                }
                System.out.println();
            }
        }


        System.out.println();

        // Druga szachownica stworzona przez prowadzącego
        for (int y = 0; y < longChess ; y++) {
            for (int x = 0; x < longChess; x++) {
                if ((x+y) % 2 == 0) {
                    System.out.print(" " + blackField);
                }else {
                    System.out.print( " " + whiteField );
                }
            }
            // tutaj przechodzi do nowej lini Y
            System.out.println();
        }
    }
}
